# sql-db-restore

## How to run

* Create a new instance of Windows Server 2016 with Sql Server 2017 pre-installed and sa password known
* Assign role to EC2 instance with permissions to access S3 Bucket
* Populate environment variables and run the powershell script e.g.:
```
$env:bucketName="vk-ptp"
$env:backupFileName="PSRestoreTest.bak"
$env:scriptFileName="SqlScript.sql"
$env:saPassword="52CXIQ!17o6*3nOt"
$env:sqlUsername="squirt-sql-user"

# Enable strong security
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

# Enable script execution
Set-ExecutionPolicy Bypass -Scope Process -Force

# Install Chocolately
iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

# Install git
choco install git -params '"/GitOnlyOnPath"' -y

# Clone The Repo
& 'C:\Program Files\Git\bin\git.exe' clone --depth=1 https://bitbucket.org/vk-sqdevops/sql-db-restore.git "C:\Squirt"

# Run the script
& ./sql-db-restore/restore-db.ps1
```

## To remove

* Verify variables below and run the powershell script:
```
$dbName="PSRestoreTest"
$hostname = (hostname)
$sqlServerName = $hostname
$saLogin="sa"
$saPassword = $env:saPassword
$sqlUsername = $env:sqlUsername


$sqlCmd = "drop database [$dbName];"
SQLCMD -S $SqlServerName -Q $sqlCmd -U $saLogin -P $saPassword

$sqlCmd = "ALTER SERVER ROLE [sysadmin] ADD MEMBER [$hostname\Administrator];"
SQLCMD -S $SqlServerName -Q $sqlCmd -U $saLogin -P $saPassword

$sqlCmd = "drop login [$hostname\Administrator];"
SQLCMD -S $SqlServerName -Q $sqlCmd -U $saLogin -P $saPassword


$sqlCmd = "drop login [$sqlUsername];"
SQLCMD -S $SqlServerName -Q $sqlCmd -U $saLogin -P $saPassword

Remove-Item -Recurse -Force $rootPath
```

## TODO:

* Sql Jobs not all are restored due to t-sql errors
