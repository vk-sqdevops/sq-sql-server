$ErrorActionPreference = "Stop"

#########################################################################################
# Variables, some are environment
#########################################################################################
# S3 Bucket & Files
$bucketName = $env:bucketName
$backupFileName = $env:backupFileName
$scriptFile = $env:scriptFileName

# Local OS
$hostname = (hostname)

# Local File System
$rootPath = "C:\Squirt\"
$backupPath = "$rootPath\Backup\"
$logPath ="$rootPath\Logs\"
$dataPath ="$rootPath\Data\"
$scriptPath ="$rootPath\Scripts\"

# Sql Server
$sqlServerName = $hostname
$saLogin = "sa"
$saPassword = $env:saPassword
$sqlUsername = $env:sqlUsername
$sqlPassword = $([system.web.security.membership]::GeneratePassword(12,6))

# Script
$outputFile = "./Output.json"
#########################################################################################

#########################################################################################
# Create folders
#########################################################################################
Write-host "Creating folders..."
# mkdir $rootPath
mkdir $backupPath
mkdir $logPath
mkdir $dataPath
mkdir $scriptPath
#########################################################################################

#########################################################################################
# Download from S3
#########################################################################################
Write-host "Downloading S3 files..."
Copy-S3Object -BucketName $bucketName -key $backupFileName -file $backupPath\$backupFileName
Copy-S3Object -BucketName $bucketName -key $scriptFile -file $scriptPath\$scriptFile
#########################################################################################

#########################################################################################
# Grant sql credentials to local administrator
#########################################################################################
Write-host "Granding access for local administrator..."
$sqlCmd = "create login [$hostname\Administrator] from windows;"
SQLCMD -S $SqlServerName -Q $sqlCmd -U $saLogin -P $saPassword

$sqlCmd = "ALTER SERVER ROLE [sysadmin] ADD MEMBER [$hostname\Administrator];"
SQLCMD -S $SqlServerName -Q $sqlCmd -U $saLogin -P $saPassword
#########################################################################################

#########################################################################################
# Restore database
#########################################################################################
Write-host "Restoring database..."
$results = SQLCMD -E -S $SqlServerName -Q "restore headeronly from disk = '$($backupPath+$backupFileName)'" -W -m 1 -s "|"|convertfrom-csv -Delimiter "|"|Select-Object -Skip 1|where{$_.databasename -ne $null}		
$DBName = $results.databasename
$results = SQLCMD -E -S $SqlServerName -Q "restore filelistonly from disk = '$($backupPath+$backupFileName)'" -W -m 1 -s "|"|convertfrom-csv -Delimiter "|"|Select-Object -Skip 1|where{$_.type -ne $null}		
           
foreach($result in $results) {
  switch($result.type){
    D {$dataname = $result.LogicalName;$dataphname = $result.PhysicalName.replace((split-path $result.PhysicalName),"").replace("\","") }
    L {$Logname = $result.LogicalName;$logphname = $result.PhysicalName.replace((split-path $result.PhysicalName),"").replace("\","") }
  }
}   

$RestoreQuery = "RESTORE DATABASE [$DBName] FROM DISK = '$($backupPath+$backupFileName)' WITH MOVE "
$RestoreQuery +="'$($dataname)' TO '$($dataPath+$dataphname)', MOVE '$($Logname)' TO '$($dataPath+$logphname)', RECOVERY, REPLACE, STATS = 10;"
Write-host "Running restore query: $($RestoreQuery)"
$restore = SQLCMD -E -S $SqlServerName -Q $RestoreQuery -W                
#########################################################################################

#########################################################################################
# Run script to create sql jobs
#########################################################################################
Write-host "Running script..."
$scriptrun = SQLCMD -E -S $SqlServerName -i $($scriptPath+$scriptFile) -W -b
#########################################################################################

#########################################################################################
# Create application user
#########################################################################################
Write-host "Creating database user..."
$sqlCmd = @"
use [$DBName];
CREATE LOGIN [$username] WITH PASSWORD = '$password';
CREATE USER [$username] FOR LOGIN [$username];
EXEC sp_addrolemember N'db_owner', N'$username';
"@

sqlcmd -E -S $SqlServerName -Q $sqlCmd
#########################################################################################

#########################################################################################
# Output
#########################################################################################
$instance = (Invoke-WebRequest http://169.254.169.254/latest/dynamic/instance-identity/document -useBasicParsing -ErrorAction SilentlyContinue).content|convertfrom-json
$string=@"
{
  "connectionString": 
  "Provider=SQLNCLI11;Server=$($instance.privateip);Database=$DBname;Uid=$username;Pwd=$password;"
}
"@
set-content $outputfile -value $string -force
get-Content $outputfile
#########################################################################################